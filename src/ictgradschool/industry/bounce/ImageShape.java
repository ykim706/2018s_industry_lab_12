package ictgradschool.industry.bounce;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {


    public ImageShape() {
        super();
    }

    public ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);

    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {
        try {
            Image image = ImageIO.read(new File("270px-Bandung_Rose.jpg")).getScaledInstance(fWidth, fHeight,0);

            painter.drawImage(image, fX, fY, null);
        } catch (IOException e) {
        }
    }


}
