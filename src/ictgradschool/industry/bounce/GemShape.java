package ictgradschool.industry.bounce;

import java.awt.*;

public class GemShape extends Shape {
    public GemShape(){super(); }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height){super(x,y,deltaX,deltaY,width,height);}

    @Override
    public void paint(Painter painter) {
        if(fWidth > 40 ){
        int xPoints[] = {(fX + 20), (fX +(fWidth - 20)), (fX + fWidth), (fX +(fWidth - 20)),(fX + 20) , fX };
        int yPoints[] = {fY, fY, (fY+(fHeight/2)),  (fY+fHeight),(fY+(fHeight)), (fY+(fHeight/2))};

        Polygon polygon = new Polygon(xPoints, yPoints, 6);
        painter.drawPolygon(polygon);
    } else{
            int xPoints[] ={fX, fX + (fWidth/2), fX + fWidth,  fX + (fWidth/2) };
            int yPoints[] ={fY+ (fHeight/2), fY ,fY + (fHeight/2), fY +fHeight};
            Polygon polygon = new Polygon(xPoints, yPoints, 4);
            painter.drawPolygon(polygon);
        }
    }
}
