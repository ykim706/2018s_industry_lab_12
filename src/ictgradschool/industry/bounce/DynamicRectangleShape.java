package ictgradschool.industry.bounce;

import java.awt.*;

public class DynamicRectangleShape extends Shape {

    private boolean isFilled;
    private Color colour;



    public DynamicRectangleShape() {
        super();
        this.colour = colour;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        this.colour = Color.black;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color colour) {
        super(x, y, deltaX, deltaY, width, height);
        this.colour = colour;
    }

    @Override
    public void paint(Painter painter) {

        if(isFilled){
            painter.drawRect(fX, fY, fWidth, fHeight);
            painter.setColor(colour);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.BLACK);
        } else {
            painter.setColor(Color.BLACK);
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
    }

//    @Override
//    public void paint(Painter painter2) {
//        painter2.setColor(Color.BLACK);
//        painter2.drawRect(fX, fY, fWidth, fHeight);
//    }



    @Override
    public void move(int width, int height) {

        int oldX = fDeltaX;
        int oldY = fDeltaY;

        super.move(width, height);

        if (oldX != fDeltaX ) {
            isFilled = true;
        } else if (oldY != fDeltaY){
            isFilled = false;
        }
    }

}
